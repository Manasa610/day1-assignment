package day4;

import day4_1.A6;
import day4_1.B;
import day4_1.C;

public class OOP6 {
			static int a = 555;
			public static void main(String[] args) {
				A6 objA = new A6();
				B objB1 = new B();
				B objB2 = new B();
				C objC1 = new C();
				B objC2 = new C();
				C objC3 = new C();
				objA.display();
				objB1.display();
				objB2.display();
				objC1.display();
				objC2.display();
				objC3.display();
			}
		}
