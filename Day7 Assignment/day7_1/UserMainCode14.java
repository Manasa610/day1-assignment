package day7_1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

public class UserMainCode14 {
	public static String convertDateFormat(String inputString) {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = dateFormat.parse(inputString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		DateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yy");
		inputString = dateFormat1.format(date);
		// System.out.println(" Converted date is : "+ inputString);
		return inputString;
	}
}
