/*Given a method with two date strings in yyyy-mm-dd format as input. Write code to find the difference between two dates in months.  
Include a class UserMainCode with a static method getMonthDifference which accepts two date strings as input.  
The return type of the output is an integer which returns the diffenece between two dates in months.  
Create a class Main which would get the input and call the static method getMonthDifference present in the UserMainCode. 
  
Sample Input 1: 
2012-03-01 
2012-04-16 
Sample Output 1: 
1 
Sample Input 2: 
2011-03-01 
2012-04-16 
Sample Output 2: 
13 */

package day7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import day7_1.UserMainCode11;

public class DifferenceInMonthsMain11 {
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the date: ");
		String string1 = null;
		try {
			string1 = br.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("Enter the date: ");
		String string2 = null;
		try {
			string2 = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(UserMainCode11.getMonthDifference(string1, string2));

	}
}