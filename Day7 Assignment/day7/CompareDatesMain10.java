/*Write a program to read  two String variables in DD-MM-YYYY.Compare the two dates and return the older date in 'MM/DD/YYYY' format. 
Include a class UserMainCode with a static method findOldDate which accepts the string values. The return type is the string. 
Create a Class Main which would be used to accept the two string values and call the static method present in UserMainCode. 
Sample Input 1: 
05-12-1987 
8-11-2010 
Sample Output 1: 
12/05/1987 */

package day7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import day7_1.UserMainCode10;

public class CompareDatesMain10 {

	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the date: ");
		String string1 = null;
		try {
			string1 = br.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("Enter the date: ");
		String string2 = null;
		try {
			string2 = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			System.out.println(UserMainCode10.findOldDate(string1, string2));
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
}
