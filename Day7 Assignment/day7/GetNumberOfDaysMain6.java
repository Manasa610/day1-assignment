
/*Include a class�UserMainCode�with a static method �getNumberOfDays� that accepts 2 integers as arguments and returns an integer. The first argument corresponds to the year and the second argument corresponds to the month code. The method returns an integer corresponding to the number of days in the month.�
Create a class Main which would get 2 integers as input and call the static method�getNumberOfDays�present in the�UserMainCode.�
Input and Output Format:�
Input consists of 2 integers that correspond to the year and month code.�
Output consists of an integer that correspond to the number of days in the month in the given year.�
Sample Input:�
2000�
1�
Sample Output:�
29�*/

package day7;

import day7_1.UserMainCode6;

public class GetNumberOfDaysMain6 {

	public static void main(String[] args) {
		System.out.println(UserMainCode6.getNumberOfDays(2000, 1));
		System.out.println(UserMainCode6.getNumberOfDays(2001, 1));

	}

}
