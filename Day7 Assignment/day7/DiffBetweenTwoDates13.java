/*13.Get two date strings as input and write code to find difference between two dates in days. 

Include a class UserMainCode with a static method getDateDifference which accepts two
date strings as input. 
The return type of the output is an integer which returns the difference between two dates
in days. 
Create a class Main which would get the input and call the static
method getDateDifference present in the UserMainCode. 
  
Sample Input 1: 
2012-03-12 
2012-03-14 
Sample Output 1: 
2 
 
Sample Input 2: 
2012-04-25 
2012-04-28 
Sample Output 2: 
3*/

package day7;

import java.text.ParseException;
import day7_1.UserMainCode13;

public class DiffBetweenTwoDates13 {
	public static void main(String[] args) throws ParseException {
		String s1 = "2012-04-25";
		String s2 = "2012-04-28";
		System.out.println(UserMainCode13.getDateDifference(s1, s2));
	}
}
