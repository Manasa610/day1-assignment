/*Given a date string in the format dd/mm/yyyy, write a program to convert the given date to the format dd-mm-yy. 
Include a class UserMainCode with a static method “convertDateFormat” that accepts a String and returns a String. 
 Create a class Main which would get a String as input and call the static method convertDateFormat present in the UserMainCode. 
Sample Input: 
12/11/1998 
  
Sample Output: 
12-11-98*/

package day7;

import day7_1.UserMainCode14;

public class ConvertDateMain14 {
	public static void main(String[] args) {
		System.out.println(UserMainCode14.convertDateFormat("12/11/1998"));
	}
}
